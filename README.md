# Model Evaluation and Validation


### Requirements

This project requires **Python** and the following Python libraries installed:

- [NumPy](http://www.numpy.org/)
- [Pandas](http://pandas.pydata.org/)
- [matplotlib](http://matplotlib.org/)
- [scikit-learn](http://scikit-learn.org/stable/)
- [turicreate](https://github.com/apple/turicreate)

You will also need to have software installed to run and execute a [Jupyter Notebook](http://ipython.org/notebook.html)

If you do not have Python installed yet, it is highly recommended that you install the [Anaconda](http://continuum.io/downloads) distribution of Python, which already has the above packages and more included. 

### Run

In a terminal or command window, navigate to the individual project folders and run:

#### Create a virtual environment
```
 pipenv install -r requirements.txt --skip-lock
```

#### Activate the virtual environment 
``` 
 pipenv shell
```

#### Launch jupyter notebook
```bash
jupyter notebook document-retrieval/people_wiki.ipynb 
```

This will open the Jupyter Notebook software and project file in your browser.

## Projects

- Pima Indians Diabetes data analysis
- Image classification
- Song recommendation system
- Document retrieval
- Product sentiment analysis