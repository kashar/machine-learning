import numpy as np

arr = np.array([[-2, -1, -3],
                [4, 5, -6],
                [3, 9, 1]])
print(repr(arr > 0))
print(repr(np.any(arr > 0, axis=0)))
print(repr(np.any(arr > 0, axis=1)))
print(repr(np.all(arr > 0, axis=1)))

print ('---------')
arr1 = np.any(np.array([[False, False],
                        [False, True]]), axis=1)

print (repr(arr1))

arr2 = np.any(np.array([[True, False],
                        [False, True]]), axis=0)
print (repr(arr2))

